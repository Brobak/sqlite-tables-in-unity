﻿using System.Collections.Generic;
using System.Data;
using SQLiteTables;

namespace SQLiteExample
{
    public class CityTable : SQLiteTable<CityEntity>
    {
        // These are just to make column names searchable for the auto complete
        public static readonly string Id = "ID";
        public static readonly string Name = "Name";
        public static readonly string Population = "Population";
        public static readonly string ZipCode = "ZipCode";

        public CityTable() : base("CityTable", Id)
        {
            CreateTable();
        }

        public override string[] columnNames => new[] {Id, Name, Population, ZipCode};

        public override void CreateTable()
        {
            Create(name)
                .WithPrimaryKey(dbInt, Id)
                .AddColumn(dbText, Name)
                .AddColumn(dbInt, Population)
                .AddColumn(dbInt, ZipCode)
                .Execute();
        }

        public override void AddData(CityEntity element)
        {
            Insert()
                .OrReplace()
                .WithValue(Id, element.id)
                .WithValue(Name, element.name)
                .WithValue(Population, element.population)
                .WithValue(ZipCode, element.zipCode)
                .Execute();
        }

        public override void DeleteData(CityEntity element)
        {
            DeleteData(element.id);
        }

        public override void UpdateData(CityEntity element)
        {
            Update()
                .WhereEquals(Id, element.id)
                .SetValue(Name, element.name)
                .SetValue(Population, element.name)
                .SetValue(ZipCode, element.zipCode)
                .Execute();
        }

        public CityEntity GetCityByName(string cityName)
        {
            return Select()
                .WhereEquals(Name, cityName)
                .ReadSingle<CityEntity>();
        }

        public IEnumerable<CityEntity> GetLargeCities()
        {
            return Select()
                .Where($"{Population} >= 100000")
                .ReadMany<CityEntity>();
        }
    }
}