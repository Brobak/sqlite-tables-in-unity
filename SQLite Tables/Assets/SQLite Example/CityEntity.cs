﻿using System.Collections;
using System.Data;
using SQLiteTables;
using UnityEngine;

namespace SQLiteExample
{
    public class CityEntity : DataEntity
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public int population { get; private set; }
        public int zipCode { get; private set; }

        public CityEntity() : base()
        { }

        public CityEntity(int id, string name, int population, int zipCode)
        {
            this.id = id;
            this.name = name;
            this.population = population;
            this.zipCode = zipCode;
        }

        public override void FromReader(IDataReader reader)
        {
            // Values are read with readNextRow set to false, since all the values are on the same row
            id = reader.ReadInt(CityTable.Id, false).Value;
            name = reader.ReadString(CityTable.Name, false);
            population = reader.ReadInt(CityTable.Population, false).Value;
            zipCode = reader.ReadInt(CityTable.ZipCode, false).Value;
        }
    }
}
