﻿using System;
using System.Collections;
using System.Collections.Generic;
using SQLiteTables;
using UnityEngine;
using UnityEngine.UI;

namespace SQLiteExample
{
    public class DatabaseTester : MonoBehaviour
    {
        public Text textField;

        private CityTable cityTable;
        private DenmarkInfoTable denmarkInfoTable;

        private void Awake()
        {
            cityTable = new CityTable();
            denmarkInfoTable = new DenmarkInfoTable(cityTable);
        }

        private void Start()
        {
            cityTable.DropTable();
            denmarkInfoTable.DropTable();
            cityTable.CreateTable();
            denmarkInfoTable.CreateTable();

            SaveCities();

            SaveInfoAboutDenmark();

            textField.text = "";

            LoadInfoAboutDenmark();

            textField.text += "\n\n";

            LoadCities();
        }

        private void SaveCities()
        {
            cityTable.AddData(new CityEntity(cityTable.GetUnusedRowID(), "Copenhagen", 632_340, 1000));
            cityTable.AddData(new CityEntity(cityTable.GetUnusedRowID(), "Aarhus", 336_411, 8000));
            cityTable.AddData(new CityEntity(cityTable.GetUnusedRowID(), "Aalborg", 140_897, 9000));
            cityTable.AddData(new CityEntity(cityTable.GetUnusedRowID(), "Odense", 200_703, 5000));
            cityTable.AddData(new CityEntity(cityTable.GetUnusedRowID(), "Esbjerg", 115_932, 6700));
            cityTable.AddData(new CityEntity(cityTable.GetUnusedRowID(), "Grindsted", 9_782, 4200));
        }

        private void SaveInfoAboutDenmark()
        {
            denmarkInfoTable.SetName("Denmark");

            CityEntity capital = cityTable.GetCityByName("Copenhagen");
            denmarkInfoTable.SetCapital(capital.id);
            denmarkInfoTable.SetPopulation(5_806_000);
            denmarkInfoTable.SetJoinedEU(new DateTime(1973, 1, 1));
        }

        private void LoadInfoAboutDenmark()
        {
            textField.text += $"{denmarkInfoTable.GetName()} has " +
                              $"{denmarkInfoTable.GetPopulation()} people and " +
                              $"the capital is {denmarkInfoTable.GetCapital().name}. " +
                              $"They joined the EU on {denmarkInfoTable.GetJoinedEU():d}";
        }

        private void LoadCities()
        {
            textField.text += "Cities in table:\n";

            foreach (CityEntity city in cityTable.GetAllData())
            {
                textField.text += $"{city.name} | {city.population} | {city.zipCode}\n";
            }

            textField.text += "\nLarge cities: ";

            foreach (CityEntity city in cityTable.GetLargeCities())
            {
                textField.text += $"{city.name} ";
            }
        }
    }
}