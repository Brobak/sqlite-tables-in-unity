﻿using System;
using SQLiteTables;

namespace SQLiteExample
{
    public class DenmarkInfoTable : SQLiteSingleEntryTable
    {
        // These are just to make column names searchable for the auto complete
        public static readonly string Name = "Name";
        public static readonly string Capital = "Capital";
        public static readonly string Population = "Population";
        public static readonly string JoinedEU = "DateOfEUJoin";

        private readonly CityTable cityTable;

        public override string[] columnNames => new[] {Name, Capital, Population};

        public DenmarkInfoTable(CityTable cityTable) : base("DenmarkInfoTable")
        {
            this.cityTable = cityTable;

            CreateTable();
        }

        public override void CreateTable()
        {
            Create(name)
                .WithPrimaryKey(dbInt, primaryKey)
                .AddColumn(dbText, Name)
                .AddColumn(dbInt, Capital)
                .AddColumn(dbInt, Population)
                .AddColumn(dbText, JoinedEU)
                .AddForeignKeyConstraint(Capital, cityTable.name, CityTable.Id)
                .Execute();
        }

        public string GetName()
        {
            CheckInit();

            return Select(Name)
                .ReadString();
        }

        public void SetName(string newName)
        {
            UpdateValue(Name, newName);
        }

        public CityEntity GetCapital()
        {
            CheckInit();

            int? capitalId = Select(Capital)
                .ReadInt();

            if (capitalId == null)
            {
                throw new NullReferenceException("No capital of Denmark has been set");
            }

            return cityTable.GetData(capitalId.Value);
        }

        public void SetCapital(int cityId)
        {
            UpdateValue(Capital, cityId);
        }

        public int GetPopulation()
        {
            CheckInit();

            return Select(Population)
                .ReadInt()
                .Value;
        }

        public void SetPopulation(int newPopulation)
        {
            UpdateValue(Population, newPopulation);
        }

        public DateTime GetJoinedEU()
        {
            CheckInit();

            return Select(JoinedEU)
                .ReadDateTime()
                .Value;
        }

        public void SetJoinedEU(DateTime newDate)
        {
            UpdateValue(JoinedEU, newDate);
        }

        protected override void CreateInitialValues()
        {
            Insert()
                .WithValue(primaryKey, 0)
                .WithValue(Name, "Denmark")
                .WithValue<int?>(Capital, null)
                .WithValue(Population, 5000000)
                .WithValue(JoinedEU, DateTime.Now)
                .Execute();
        }
    }
}