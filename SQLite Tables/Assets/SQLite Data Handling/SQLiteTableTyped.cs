﻿using System.Collections.Generic;
using System.Data;

namespace SQLiteTables
{
    public abstract class SQLiteTable<T> : SQLiteTable, ITable<T> where T : DataEntity, new()
    {
        protected SQLiteTable(string name, string primaryKey) : base(name, primaryKey)
        {
        }

        public abstract void AddData(T element);

        public T GetData<TKey>(TKey key)
        {
            return Select()
                .WhereEquals(primaryKey, key)
                .ReadSingle<T>();
        }

        public int GetUnusedRowID()
        {
            if (GetRowCount() > 0)
            {
                return GetMaxInt(primaryKey) + 1;
            }
            else
            {
                return int.MinValue;
            }
        }

        public IEnumerable<T> GetAllData()
        {
            return Select().ReadMany<T>();
        }

        public abstract void DeleteData(T element);

        public void DeleteData<TKey>(TKey key)
        {
            Delete()
                .WhereEquals(primaryKey, key)
                .Execute();
        }

        public abstract void UpdateData(T element);

        public int GetMaxInt(string column)
        {
            return Max(column).ReadInt().Value;
        }

        public float GetMaxReal(string column)
        {
            return Max(column).ReadFloat().Value;
        }

        public int GetMinInt(string column)
        {
            return Min(column).ReadInt().Value;
        }

        public float GetMinReal(string column)
        {
            return Min(column).ReadFloat().Value;
        }
    }
}