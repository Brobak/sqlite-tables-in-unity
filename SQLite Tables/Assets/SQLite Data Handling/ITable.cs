﻿using System.Collections.Generic;

namespace SQLiteTables
{
    public interface ITable<T> where T : DataEntity
    {
        string[] columnNames { get; }

        void CreateTable();

        void DropTable();

        void AddData(T element);

        T GetData<TKey>(TKey key);

        int GetRowCount();

        int GetUnusedRowID();

        IEnumerable<T> GetAllData();

        void DeleteData(T element);

        void DeleteData<TKey>(TKey key);

        void UpdateData(T element);
    }
}