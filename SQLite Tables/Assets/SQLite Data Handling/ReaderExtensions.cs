﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

namespace SQLiteTables
{
    public static class ReaderExtensions
    {
        public static T ReadSingle<T>(this IDataReader reader) where T : DataEntity, new()
        {
            reader.Read();

            T entity = new T();
            entity.FromReader(reader);

            return entity;
        }

        public static IEnumerable<T> ReadMany<T>(this IDataReader reader) where T : DataEntity, new()
        {
            while (reader.Read())
            {
                T entity = new T();
                entity.FromReader(reader);

                yield return entity;
            }
        }

        public static int? ReadInt(this IDataReader reader, string key, bool readNextRow = true)
        {
            int ordinal = reader.GetOrdinal(key);

            return ReadInt(reader, ordinal, readNextRow);
        }

        public static int? ReadInt(this IDataReader reader, bool readNextRow = true)
        {
            return ReadInt(reader, 0, readNextRow);
        }

        public static IEnumerable<int?> ReadInts(this IDataReader reader)
        {
            while (reader.Read())
            {
                yield return ReadInt(reader, 0, false);
            }
        }

        private static int? ReadInt(IDataReader reader, int ordinal, bool readNextRow)
        {
            if (readNextRow)
            {
                reader.Read();
            }

            if (reader.IsDBNull(ordinal))
            {
                return null;
            }

            return reader.GetInt32(ordinal);
        }

        public static float? ReadFloat(this IDataReader reader, string key, bool readNextRow = true)
        {
            int ordinal = reader.GetOrdinal(key);

            return ReadFloat(reader, ordinal, readNextRow);
        }

        public static float? ReadFloat(this IDataReader reader, bool readNextRow = true)
        {
            return ReadFloat(reader, 0, readNextRow);
        }

        public static IEnumerable<float?> ReadFloats(this IDataReader reader)
        {
            while (reader.Read())
            {
                yield return ReadFloat(reader, 0, false);
            }
        }

        private static float? ReadFloat(IDataReader reader, int ordinal, bool readNextRow)
        {
            if (readNextRow)
            {
                reader.Read();
            }

            if (reader.IsDBNull(ordinal))
            {
                return null;
            }

            return reader.GetFloat(ordinal);
        }

        public static string ReadString(this IDataReader reader, string key, bool readNextRow = true)
        {
            int ordinal = reader.GetOrdinal(key);

            return ReadString(reader, ordinal, readNextRow);
        }

        public static string ReadString(this IDataReader reader, bool readNextRow = true)
        {
            return ReadString(reader, 0, readNextRow);
        }

        public static IEnumerable<string> ReadStrings(this IDataReader reader)
        {
            while (reader.Read())
            {
                yield return ReadString(reader, 0, false);
            }
        }

        private static string ReadString(IDataReader reader, int ordinal, bool readNextRow)
        {
            if (readNextRow)
            {
                reader.Read();
            }

            if (reader.IsDBNull(ordinal))
            {
                return null;
            }

            return reader.GetString(ordinal);
        }

        public static DateTime? ReadDateTime(this IDataReader reader, string key, bool readNextRow = true)
        {
            int ordinal = reader.GetOrdinal(key);

            return ReadDateTime(reader, ordinal, readNextRow);
        }

        public static DateTime? ReadDateTime(this IDataReader reader, bool readNextRow = true)
        {
            return ReadDateTime(reader, 0, readNextRow);
        }

        public static IEnumerable<DateTime?> ReadDateTimes(this IDataReader reader)
        {
            while (reader.Read())
            {
                yield return ReadDateTime(reader, 0, false);
            }
        }

        private static DateTime? ReadDateTime(IDataReader reader, int ordinal, bool readNextRow)
        {
            if (readNextRow)
            {
                reader.Read();
            }

            if (reader.IsDBNull(ordinal))
            {
                return null;
            }

            string dateString = reader.GetString(ordinal);

            return ConvertUnixEpochTimeStamp(dateString);
        }

        private static DateTime ConvertUnixEpochTimeStamp(string dateString)
        {
            return DateTime.Parse(dateString);
        }
    } 
}
