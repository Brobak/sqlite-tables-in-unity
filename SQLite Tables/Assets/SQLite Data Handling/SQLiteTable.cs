﻿using System;
using System.Collections.Generic;

namespace SQLiteTables
{
    public abstract class SQLiteTable : SQLiteHelper
    {
        public readonly string name;

        public virtual string primaryKey { get; }

        public abstract string[] columnNames { get; }

        protected SQLiteTable(string name, string primaryKey)
        {
            this.name = name;
            this.primaryKey = primaryKey;
        }

        public abstract void CreateTable();

        public void DropTable()
        {
            Drop(name).Execute();
        }

        public int GetRowCount()
        {
            return Count().ReadInt().Value;
        }

        public SQLiteInsertCommand Insert()
        {
            return InsertInto(name);
        }

        public SQLiteDeleteCommand Delete()
        {
            return Delete(name);
        }

        public SQLiteSelectQuery Select(params string[] selectedColumns)
        {
            return Select(name, selectedColumns);
        }

        public SQLiteUpdateCommand Update()
        {
            return Update(name);
        }

        public SQLiteSelectQuery Count()
        {
            return Count(name);
        }

        public SQLiteSelectQuery Max(string columnName)
        {
            return Max(name, columnName);
        }

        public SQLiteSelectQuery Min(string columnName)
        {
            return Min(name, columnName);
        }
    }
}