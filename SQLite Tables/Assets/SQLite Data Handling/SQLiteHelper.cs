﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine;

namespace SQLiteTables
{
    public class SQLiteHelper
    {
        private static string connectionString = $"URI=file:{Application.persistentDataPath}/UnityDatabase.sqlite3";

        protected static IDbConnection OpenConnection()
        {
            var dbConnection = new SqliteConnection(connectionString);
            dbConnection.Open();

            return dbConnection;
        }

        private static void SetConnectionString(string dbName)
        {
            connectionString = $"URI=file:{Application.persistentDataPath}/{dbName}.sqlite3";
        }

        public static void SwitchToTestingContext()
        {
            SetConnectionString("UnityTestingDatabase");
        }

        protected static string dbInt => "INTEGER";

        protected static string dbReal => "REAL";

        protected static string dbText => "TEXT";

        protected static string notNull => "NOT NULL";

        protected CustomSQLiteCommand Execute(string command)
        {
            return new CustomSQLiteCommand(command);
        }

        protected CustomSQLiteQuery Query(string query)
        {
            return new CustomSQLiteQuery(query);
        }

        protected SQLiteCreateTableCommand Create(string tableName)
        {
            return new SQLiteCreateTableCommand(tableName);
        }

        protected SQLiteDropTableCommand Drop(string tableName)
        {
            return new SQLiteDropTableCommand(tableName);
        }

        protected SQLiteInsertCommand InsertInto(string tableName)
        {
            return new SQLiteInsertCommand(tableName);
        }

        protected SQLiteDeleteCommand Delete(string tableName)
        {
            return new SQLiteDeleteCommand(tableName);
        }

        protected SQLiteSelectQuery Select(string tableName, params string[] selectedColumns)
        {
            return new SQLiteSelectQuery(tableName, selectedColumns);
        }

        protected SQLiteUpdateCommand Update(string tableName)
        {
            return new SQLiteUpdateCommand(tableName);
        }

        protected SQLiteSelectQuery Count(string tableName)
        {
            return new SQLiteSelectQuery(tableName, "COUNT(*)");
        }

        protected SQLiteSelectQuery Max(string tableName, string columnName)
        {
            return new SQLiteSelectQuery(tableName, $"MAX({columnName})");
        }

        protected SQLiteSelectQuery Min(string tableName, string columnName)
        {
            return new SQLiteSelectQuery(tableName, $"MIN({columnName})");
        }

        protected static string StringifyValue<TValue>(TValue o)
        {
            if (o == null)
            {
                return "NULL";
            }

            switch (o)
            {
                case DateTime time:
                    return $"'{ConvertDateTime(time)}'";
                case bool b:
                    return b ? "1" : "0";
                case string s:
                    return $"'{o}'";
                default:
                    return o.ToString();
            }
        }

        protected static string ConvertDateTime(DateTime dateTime)
        {
            return dateTime.ToString("s");
        }
    } 
}
