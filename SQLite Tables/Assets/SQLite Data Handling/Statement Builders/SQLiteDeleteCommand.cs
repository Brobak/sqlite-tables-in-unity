﻿using System.Collections.Generic;
using System.Linq;

namespace SQLiteTables
{
    public class SQLiteDeleteCommand : SQLiteCommand
    {
        private readonly string table;
        private readonly List<string> constraints = new List<string>();

        public SQLiteDeleteCommand(string table)
        {
            this.table = table;
        }

        public override string ToSQLite()
        {
            string whereClause = "";
            if (constraints.Any())
            {
                whereClause = $" WHERE ( {string.Join(", ", constraints)} )";
            }

            return $"DELETE FROM {table}{whereClause}";
        }

        public SQLiteDeleteCommand Where(string constraint)
        {
            constraints.Add(constraint);

            return this;
        }

        public SQLiteDeleteCommand WhereEquals<TValue>(string column, TValue value)
        {
            constraints.Add($"{column} = {StringifyValue(value)}");

            return this;
        }
    }
}