﻿using System.Collections.Generic;
using System.Linq;

namespace SQLiteTables
{
    public class SQLiteUpdateCommand : SQLiteCommand
    {
        private readonly string table;
        private readonly Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
        private readonly List<string> constraints = new List<string>();

        public SQLiteUpdateCommand(string table)
        {
            this.table = table;
        }

        public override string ToSQLite()
        {
            string whereClause = "";
            if (constraints.Any())
            {
                whereClause = $" WHERE ( {string.Join(", ", constraints)} )";
            }

            return $"UPDATE {table} " +
                   $"SET {GetValueSetters()}" +
                   $"{whereClause}";
        }

        private string GetValueSetters()
        {
            var valuePairs = keyValuePairs
                .Select(kvp => $"{kvp.Key} = {kvp.Value}");
            return string.Join(", ", valuePairs);
        }

        public SQLiteUpdateCommand SetValue<TValue>(string key, TValue value)
        {
            keyValuePairs.Add(key, StringifyValue(value));

            return this;
        }

        public SQLiteUpdateCommand Where(string constraint)
        {
            constraints.Add(constraint);

            return this;
        }

        public SQLiteUpdateCommand WhereEquals<TValue>(string column, TValue value)
        {
            constraints.Add($"{column} = {StringifyValue(value)}");

            return this;
        }
    }
}