﻿using System.Collections.Generic;

namespace SQLiteTables
{
    public class SQLiteInsertCommand : SQLiteCommand
    {
        private readonly string table;
        private readonly Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

        public SQLiteInsertCommand(string table)
        {
            this.table = table;
        }

        public override string ToSQLite()
        {
            return $"INSERT INTO {table} " +
                   $"( {string.Join(", ", keyValuePairs.Keys)} ) " +
                   $"VALUES ( {string.Join(", ", keyValuePairs.Values)} )";
        }

        public SQLiteInsertCommand WithValue<TValue>(string key, TValue value)
        {
            keyValuePairs.Add(key, StringifyValue(value));

            return this;
        }

        public SQLiteInsertOrReplaceCommand OrReplace()
        {
            return new SQLiteInsertOrReplaceCommand(table, keyValuePairs);
        }
    }
}