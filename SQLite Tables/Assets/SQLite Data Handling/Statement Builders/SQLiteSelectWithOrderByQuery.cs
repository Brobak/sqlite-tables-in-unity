﻿namespace SQLiteTables
{
    public class SQLiteSelectWithOrderByQuery : SQLiteQuery
    {
        private readonly SQLiteCreateTableCommand innerCommand;
        private readonly string orderingTerm;

        public SQLiteSelectWithOrderByQuery(SQLiteCreateTableCommand innerCommand, string orderingTerm)
        {
            this.innerCommand = innerCommand;
            this.orderingTerm = orderingTerm;
        }

        public override string ToSQLite()
        {
            return $"{innerCommand.ToSQLite()} " +
                   $"ORDER BY {orderingTerm}";
        }
    }
}