﻿namespace SQLiteTables
{
    public class SQLiteDropTableCommand : SQLiteCommand
    {
        private readonly string table;

        public SQLiteDropTableCommand(string table)
        {
            this.table = table;
        }

        public override string ToSQLite()
        {
            return $"DROP TABLE IF EXISTS {table}";
        }
    }
}