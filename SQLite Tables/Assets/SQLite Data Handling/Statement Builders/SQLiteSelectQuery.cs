﻿using System.Collections.Generic;
using System.Linq;

namespace SQLiteTables
{
    public class SQLiteSelectQuery : SQLiteQuery
    {
        private readonly string table;
        private readonly string[] selectedColumns;
        private readonly List<string> constraints = new List<string>();

        public SQLiteSelectQuery(string table, params string[] selectedColumns)
        {
            this.table = table;
            this.selectedColumns = selectedColumns;
        }

        public override string ToSQLite()
        {
            string selection = selectedColumns.Any() ? 
                string.Join(", ", selectedColumns) : 
                "*";

            string whereClause = "";
            if (constraints.Any())
            {
                whereClause = $" WHERE ( {string.Join(", ", constraints)} )";
            }

            return $"SELECT {selection} FROM {table}{whereClause}";
        }

        public SQLiteSelectQuery Where(string constraint)
        {
            constraints.Add(constraint);

            return this;
        }

        public SQLiteSelectQuery WhereEquals<TValue>(string column, TValue value)
        {
            constraints.Add($"{column} = {StringifyValue(value)}");

            return this;
        }
    }
}