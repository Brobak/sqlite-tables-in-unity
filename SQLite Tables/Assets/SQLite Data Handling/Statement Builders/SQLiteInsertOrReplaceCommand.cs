﻿using System.Collections.Generic;

namespace SQLiteTables
{
    public class SQLiteInsertOrReplaceCommand : SQLiteCommand
    {
        private readonly string table;
        private readonly Dictionary<string, string> keyValuePairs;

        public SQLiteInsertOrReplaceCommand(string table, Dictionary<string, string> keyValuePairs)
        {
            this.table = table;
            this.keyValuePairs = new Dictionary<string, string>(keyValuePairs);
        }

        public override string ToSQLite()
        {
            return $"INSERT OR REPLACE INTO {table} " +
                   $"( {string.Join(", ", keyValuePairs.Keys)} ) " +
                   $"VALUES ( {string.Join(", ", keyValuePairs.Values)} )";
        }

        public SQLiteInsertOrReplaceCommand WithValue<TValue>(string key, TValue value)
        {
            keyValuePairs.Add(key, StringifyValue(value));

            return this;
        }
    }
}