﻿using System.Collections.Generic;
using System.Linq;

namespace SQLiteTables
{
    public class SQLiteCreateCommandWithPrimaryKey : SQLiteCommand
    {
        private readonly SQLiteCreateTableCommand innerCommand;
        private readonly Dictionary<string, string> columnKeyTypes = new Dictionary<string, string>();
        private readonly List<string> foreignKeyConstraints = new List<string>();

        public SQLiteCreateCommandWithPrimaryKey(SQLiteCreateTableCommand innerCommand, string type, string keyName)
        {
            this.innerCommand = innerCommand;
            columnKeyTypes.Add(keyName, $"{type} PRIMARY KEY");
        }

        public override string ToSQLite()
        {
            return $"{innerCommand.ToSQLite()} " +
                   $"( " +
                   $"{string.Join(", ", GetTableColumnPairs())}" +
                   $"{GetForeignKeyConstraints()}" +
                   $" )";
        }

        private IEnumerable<string> GetTableColumnPairs()
        {
            return columnKeyTypes.Select(kvp => $"{kvp.Key} {kvp.Value}");
        }

        private string GetForeignKeyConstraints()
        {
            string result = "";

            foreach (string constraint in foreignKeyConstraints)
            {
                result += ", " + constraint;
            }

            return result;
        }

        public SQLiteCreateCommandWithPrimaryKey AddColumn(string type, string columnName, string constraint = "")
        {
            string typeSpecifier = type;
            if (!string.IsNullOrEmpty(constraint))
            {
                typeSpecifier += " " + constraint;
            }

            columnKeyTypes.Add(columnName, typeSpecifier);

            return this;
        }

        public SQLiteCreateCommandWithPrimaryKey AddForeignKeyConstraint(string column, string foreignTable, string foreignKey)
        {
            string constraint = $"FOREIGN KEY({column}) " +
                                $"REFERENCES {foreignTable}" +
                                $"({foreignKey})";

            foreignKeyConstraints.Add(constraint);

            return this;
        }
    }
}