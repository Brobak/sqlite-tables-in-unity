﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Mono.Data.Sqlite;
using UnityEngine;

namespace SQLiteTables
{
    public abstract class SQLiteQuery : SQLiteStatement
    {
        public T ReadSingle<T>() where T : DataEntity, new()
        {
            return Execute(reader => reader.ReadSingle<T>());
        }

        public IEnumerable<T> ReadMany<T>() where T : DataEntity, new()
        {
            return Execute(reader => reader.ReadMany<T>().ToList());
        }

        public int? ReadInt(string key)
        {
            return Execute(reader => reader.ReadInt(key));
        }

        public int? ReadInt()
        {
            return Execute(reader => reader.ReadInt());
        }

        public IEnumerable<int?> ReadInts()
        {
            return Execute(reader => reader.ReadInts().ToList());
        }

        public float? ReadFloat(string key)
        {
            return Execute(reader => reader.ReadFloat(key));
        }
         
        public float? ReadFloat()
        {
            return Execute(reader => reader.ReadFloat());
        }

        public IEnumerable<float?> ReadFloats()
        {
            return Execute(reader => reader.ReadFloats().ToList());
        }

        public string ReadString(string key)
        {
            return Execute(reader => reader.ReadString(key));
        }

        public string ReadString()
        {
            return Execute(reader => reader.ReadString());
        }

        public IEnumerable<string> ReadStrings()
        {
            return Execute(reader => reader.ReadStrings().ToList());
        }

        public DateTime? ReadDateTime(string key)
        {
            return Execute(reader => reader.ReadDateTime(key));
        }

        public DateTime? ReadDateTime()
        {
            return Execute(reader => reader.ReadDateTime());
        }

        public IEnumerable<DateTime?> ReadDateTimes()
        {
            return Execute(reader => reader.ReadDateTimes().ToList());
        }

        private T Execute<T>(Func<IDataReader, T> read)
        {
            using (var dbConnection = OpenConnection())
            {
                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = ToSQLite();

                    try
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            return read(reader);
                        }
                    }
                    catch (SqliteException e)
                    {
                        Debug.LogError("Could not Execute Command: " + command.CommandText
                                                                     + "\nResulted in error: " + e.Message);

                        throw e;
                    }
                }
            }
        }
    }
}