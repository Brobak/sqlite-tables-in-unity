﻿namespace SQLiteTables
{
    public class SQLiteCreateTableCommand : SQLiteCommand
    {
        private readonly string table;

        public SQLiteCreateTableCommand(string table)
        {
            this.table = table;
        }

        public override string ToSQLite()
        {
            return $"CREATE TABLE IF NOT EXISTS {table}";
        }

        public SQLiteCreateCommandWithPrimaryKey WithPrimaryKey(string type, string keyName)
        {
            return new SQLiteCreateCommandWithPrimaryKey(this, type, keyName);
        }
    }
}