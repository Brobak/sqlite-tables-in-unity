﻿namespace SQLiteTables
{
    public class CustomSQLiteQuery : SQLiteQuery
    {
        private readonly string command;

        public CustomSQLiteQuery(string command)
        {
            this.command = command;
        }

        public override string ToSQLite()
        {
            return command;
        }
    }
}