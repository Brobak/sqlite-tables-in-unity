﻿using System.Collections;

namespace SQLiteTables
{
    public abstract class SQLiteStatement : SQLiteHelper
    {
        public abstract string ToSQLite();
    }
}
