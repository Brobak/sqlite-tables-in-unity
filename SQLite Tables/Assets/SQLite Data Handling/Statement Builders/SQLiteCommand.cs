﻿using System.Data;
using Mono.Data.Sqlite;
using UnityEngine;

namespace SQLiteTables
{
    public abstract class SQLiteCommand : SQLiteStatement
    {
        public void Execute()
        {
            using (var dbConnection = OpenConnection())
            {
                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = ToSQLite();

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqliteException e)
                    {
                        Debug.LogError("Could not Execute Command: " + command.CommandText
                                                                     + "\nResulted in error: " + e.Message);

                        throw e;
                    }
                }
            }
        }
    }
}