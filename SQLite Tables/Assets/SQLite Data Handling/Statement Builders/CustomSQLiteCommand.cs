﻿namespace SQLiteTables
{
    public class CustomSQLiteCommand : SQLiteCommand
    {
        private readonly string command;

        public CustomSQLiteCommand(string command)
        {
            this.command = command;
        }

        public override string ToSQLite()
        {
            return command;
        }
    }
}