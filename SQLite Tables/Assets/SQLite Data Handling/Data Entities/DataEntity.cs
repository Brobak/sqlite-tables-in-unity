﻿using UnityEngine;
using System.Collections;
using System.Data;

namespace SQLiteTables
{
    public abstract class DataEntity
    {
        protected DataEntity()
        { }

        public abstract void FromReader(IDataReader reader);
    }
}