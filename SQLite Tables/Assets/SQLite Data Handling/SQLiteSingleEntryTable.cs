﻿using System.Data;

namespace SQLiteTables
{
    public abstract class SQLiteSingleEntryTable : SQLiteTable
    {
        protected static readonly string Id = "ID";
        public override string primaryKey => Id;

        private bool isInitialized = false;

        protected SQLiteSingleEntryTable(string name) : base(name, Id)
        {
        }

        protected void UpdateValue<TValue>(string column, TValue newValue)
        {
            CheckInit();

            Update()
                .WhereEquals(Id, 0)
                .SetValue(column, newValue)
                .Execute();
        }

        protected abstract void CreateInitialValues();

        protected void CheckInit()
        {
            if (!isInitialized)
            {
                isInitialized = true;
                if (GetRowCount() == 0)
                {
                    CreateInitialValues();
                }
            }
        }
    }
}